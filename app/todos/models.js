var db = require('mongoose');

var ObjectId = db.Schema.ObjectId;
var Schema = new(db.Schema)({});

Schema.methods.toJSON = function() {
  return {
    id: this._id,
    title: this.title,
    done: this.done,
    user: this.user.toJSON()
  };
};

Schema.statics.add = function(args, fn) {

};

Schema.statics.get = function(args, fn) {

};

Schema.statics.fetch = function(args, fn) {

};

Schema.statics.change = function(args, fn) {

};

Schema.statics.remove = function(args, fn) {

};

var Todo = db.model('Todo', Schema);
